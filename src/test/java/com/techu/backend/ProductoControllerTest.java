package com.techu.backend;

import com.techu.backend.controller.ProductoController;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductoControllerTest {

    ProductoController proController = new ProductoController();

    @Test
    public void testCaseV1() {
        assertEquals("API REST Tech U! v1.0", proController.index());
    }

    //@Test
    //public void testCaseV2() {
    //    assertEquals("API REST Tech U! v2.0", proController.index());
    //}

    //@Test
    //public void testCaseV3() {
    //    assertEquals("API REST Tech U! v3.0", proController.index());
    //}
}
