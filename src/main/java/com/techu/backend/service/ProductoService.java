package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.ProductoPrecioModel;
import com.techu.backend.model.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {

    private List<ProductoModel> productoList = new ArrayList<>();

    public ProductoService() {
        productoList.add(new ProductoModel("1","Producto 001", 101.5));
        productoList.add(new ProductoModel("2","Producto 002", 102.5));
        productoList.add(new ProductoModel("3","Producto 003", 103.5));
        productoList.add(new ProductoModel("4","Producto 004", 104.5));
        productoList.add(new ProductoModel("5","Producto 005", 105.5));
        List<UserModel> users = new ArrayList<>();
        users.add(new UserModel("001"));
        users.add(new UserModel("003"));
        users.add(new UserModel("005"));
        productoList.get(1).setUsers(users);
    }

    //READ productos
    public List<ProductoModel> getProductos() {
        return productoList;
    }

    //READ producto por ID
    public ProductoModel getProductoById(String id) {
        int idx = Integer.parseInt(id);
        int index = getIndex(idx);
        if(index >= 0) {
            return productoList.get(index);
        }
        return null;
    }

    //CREATE producto
    public ProductoModel addProducto(ProductoModel nuevoProducto) {
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }

    //UPDATE producto
    public ProductoModel updateProductoById(String id, ProductoModel newPro) {
        int idx = Integer.parseInt(id);
        int index = getIndex(idx);
        if (index >= 0) {
            productoList.set(index, newPro);
            return productoList.get(index);
        }
        return null;
    }

    //PATCH producto precio
    public ProductoModel updateProductoPrecioById(String id, double newPrecio) {
        int idx = Integer.parseInt(id);
        int index = getIndex(idx);

        if (index >= 0) {
            ProductoModel pr = productoList.get(index);
            pr.setPrecio(newPrecio);
            productoList.set(index, pr);
            return productoList.get(index);
        }
        return null;
    }

    //DELETE producto
    public void removeProductoById(String id) {
        int idx = Integer.parseInt(id);
        int index = getIndex(idx);
        if (index >= 0) {
            productoList.remove(index);
        }
    }

    //Devuelve lista de productos con un precio mayor
    public List<ProductoModel> getMayores(int max) {
        List<ProductoModel> mayores = new ArrayList<>();
        for (int i=0; i < productoList.size(); i++) {
            if (productoList.get(i).getPrecio() > max) {
                mayores.add(productoList.get(i));
            }
        }
        return mayores;
    }

    //Devuelve lista de productos sin clientes
    public List<ProductoModel> getProductosSinClientes() {
        List<ProductoModel> sinClientes = new ArrayList<>();
        for (int i=0; i < productoList.size(); i++) {
            if (productoList.get(i).getUsers() == null) {
                sinClientes.add(productoList.get(i));
            }
        }
        return sinClientes;
    }

    // Devuelve la posicion de un producto en productoList
    public int getIndex(int id) {
        int i=0;
        while(i<productoList.size()) {
            if(Integer.parseInt(productoList.get(i).getId()) == id) {
                return(i);
            }
            i++;
        }
        return -1;
    }
}
