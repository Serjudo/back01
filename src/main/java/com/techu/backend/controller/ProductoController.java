package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.ProductoPrecioModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins ="*", methods = {RequestMethod.GET, RequestMethod.POST})
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("")
    public String index() {
        return "API REST Tech U! v1.0";
    }

    //GET Todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.getProductos();
    }

    //GET producto por ID (instancia)
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoById(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null) {
            //No existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND); //Codigo HTTP 404
        }
        //return ResponseEntity.ok(pr);
        return new ResponseEntity<>(pr, HttpStatus.OK); //Codigo HTTP 200
    }

    //GET subrecurso
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getUsers(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null) {
            //No existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND); //Codigo HTTP 404
        }
        if(pr.getUsers()!=null) {
            return ResponseEntity.ok(pr.getUsers());
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //GET productos mayores que un precio pasado por parametro
    @GetMapping("/productos/mayores")
    public List<ProductoModel> getMayores(@RequestParam(name="pmax") int max) {
        return productoService.getMayores(max);
    }

    //GET productos mayores que un precio pasado por parametro
    @GetMapping("/productos/sinclientes")
    public List<ProductoModel> getProductosSinClientes() {
        return productoService.getProductosSinClientes();
    }

    //POST para crear un producto
    @PostMapping("/productos")
    public ResponseEntity<String> postProducto(@RequestBody ProductoModel newProduct) {
        productoService.addProducto(newProduct);
        return new ResponseEntity<String>("Producto creado correctamente", HttpStatus.CREATED);
    }

    //PUT para actualizar un producto
    @PutMapping("/productos/{id}")
    public ResponseEntity putProductoById(@PathVariable String id, @RequestBody ProductoModel productoModel) {
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null) {
            //No existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND); //Codigo HTTP 404
        }
        productoService.updateProductoById(id, productoModel);
        return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK); //Codigo HTTP 200
    }

    //DELETE para borrar un producto
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProductById(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND); //Codigo HTTP 404
        }
        productoService.removeProductoById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); //Codigo HTTP 204
    }

    //PATCH para actualizar parcialemnte un producto
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoById(@PathVariable String id, @RequestBody ProductoPrecioModel productoPrecioModel) {
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null) {
            //No existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND); //Codigo HTTP 404
        }
        productoService.updateProductoPrecioById(id, productoPrecioModel.getPrecio());
        return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK); //Codigo HTTP 200
    }

    //    @GetMapping("/params")
    //    public String getParameterList(@RequestParam(name="param") String[] paramList) {
    //        String msg = "";
    //        int i = 0;
    //        if (paramList == null) {
    //            msg = "Lista de parámetros vacía.";
    //        } else {
    //            for (String param : paramList) {
    //                msg += "param[" + i + "] = " + param + "\n";
    //                i++;
    //            }
    //        }
    //        return msg;
    //    }

}
